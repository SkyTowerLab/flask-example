from flask import Flask, render_template, jsonify, request
from helper import really_complex

app = Flask(__name__)  # this will tell flask how it is run (thread related)

# http://127.0.0.1:5000/
@app.route("/")
def home():
    return render_template("apiDocs.html")  # it has to always return


# http://127.0.0.1:5000/square/3
@app.route("/square/<num>")
def square(num):
    return jsonify(int(num) * int(num))


# http://127.0.0.1:5000/square2/3
@app.route("/square2/<int:num>")
def square2(num):
    return jsonify(num * num)


# http://127.0.0.1:5000/square?num=3
@app.route("/square")
def square3():
    num = int(request.args.get("num"))
    return jsonify(num * num)


# http://127.0.0.1:5000/complicated
# body:
# {
#     "num": 3
# }
@app.route("/complicated", methods=["GET", "POST"])  # we can declare multiple method
def complicated():
    data = request.json  # this get the body json to a dict
    print(request.method)
    if request.method == "GET":
        return jsonify(
            really_complex(
                int(data["num"])
            )  # imagine there is a ton happening here, I usually have helpers to take away much of the logic away from the routes files
        )  # stacking function just because :D
    else:
        return jsonify(0)


# === Error Handling ===#

# 403, I don't think we will get this for this course, unless we try to auth
@app.errorhandler(403)
def forbidden(e):
    return (
        jsonify({"message": "Forbidden", "error": str(e), "data": None}),
        403,
    )  # return has 2 arguments, the jsonify, StatusCode
    # we could render a pretty nice html when this happens


# http://127.0.0.1:5000/this-is-an-error
@app.errorhandler(404)
def wrongEndpoint(e):
    return (
        jsonify({"message": "Endpoint Not Found", "error": str(e), "data": None}),
        404,
    )


# this is wrong method, for example when you try to post to a route that only accept get
@app.errorhandler(405)
def wrongEndpoint(e):
    return (
        jsonify({"message": "Method not allowed", "error": str(e), "data": None}),
        405,
    )


if __name__ == "__main__":
    app.run(
        debug=True
    )  # debug True = to nodemon but better :D | host and port can be past here too, set host=0.0.0.0 to open it up to the world
